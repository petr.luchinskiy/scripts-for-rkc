﻿import pypyodbc
import string
import win32com.client
Excel = win32com.client.Dispatch("Excel.Application")
import time
import functools

start_time = time.time()

mySQLServer = "laptem"
myDataBase = "naselenie"

connection = pypyodbc.connect('Driver={SQL Server};'
								'Server=' + mySQLServer + ';'
								'Database=' + myDataBase + ';')

cursor = connection.cursor()

mySQLQuery = ("""
				SELECT Улица,УлицаИд
				FROM Улицы
				""")

cursor.execute(mySQLQuery)

resultsUl = dict(cursor.fetchall())

wb = Excel.Workbooks.Open('D:/Python/rkc/zachety_ssgpo/work_file.xls')
sheet = wb.Worksheets(u'Рудный')
sheetKach = wb.Worksheets(u'Качар')
sheetItog = wb.Worksheets(u'итоги')	

#sheetKach.Cells(2,1).value = [r[0].value for r in sheet.Range("A2:K2")]

podraz = {'2.0' : 'Сарбайское РУ', 
'4.0' : 'ССГПО',
'6.0' : 'УГЖДТ', 
'8.0' : 'АТУ', 
'9.0' : 'Управление объединения',
'10.0' : 'Информационный центр',
'11.0' : 'ТЭЦ', 
'15.0' : 'УБВР', 
'16.0' : 'Складское хозяйство', 
'20.0' : 'ЦСиП', 
'22.0' : 'УПХиС', 
'23.0' : 'Качарское РУ', 
'24.0' : 'Шахта Соколовская', 
'40.0' : 'ОТК', 
'41.0' : 'СМУ', 
'51.0' : 'ФПО', 
'52.0' : 'ФРПО', 
'55.0' : 'УРТО', 
'61.0' : 'Качарский горнодобычной цех',
'62.0' : 'Качарский транспортный цех',
'63.0' : 'Качарский транспортный цех',
'66.0' : 'КУГЖДТ', 
'67.0' : 'МПЗ', 
'70.0' : 'Негосударственная противопожарная служба', 
'74.0' : 'КР Горнодобычной цех', 
'75.0' : 'КР Транспортный цех', 
'76.0' : 'КР Теплоцент', 
'82.0' : 'Цех автоматизации и инфор.технологий', 
'93.0' : 'ТОО Металлург', 
}

valc = {"Ә": "А", "І": "И", "Ң": "Н", "Ғ": "Г", "Ү": "У", "Ұ": "У", "Қ": "К", "Ө": "О", "Һ": "Х"}

to = ''.join([str(i) for i in valc.values()])
frm = ''.join([str(i) for i in valc.keys()])


kachar = ['КАЧАР 1-й микрорайон','КАЧАР 2-й микрорайон','КАЧАР 3-й микрорайон', 'КАЧАР 3-й микрорайон', 'КАЧАР Привольная', 'КАЧАР Степная']

slovul = {
'8 МАРТА' : ['ВОСЬМОГО МАРТА'],
'А-АТИНСКИЙ' : ['АЛМА'],
'Б-ТАРАСОВА' : ['ТАРАСОВА'],
'ВОСЬМОЙ' : ['ВОСЬМОЙ', 'ПЕРЕУЛОК 8', 'ПЕЕУЛОК 8'],
'ВТОРОЙ' : ['ВТОРОЙ', 'ПЕРЕУЛОК 2'],
'И-ФРАНКО' : ['ФРАНКО'],
'КОМСОМОЛЬСКИЙ' : ['КОМСОМОЛЬСК', 'КОМСОЛЬСКИЙ'],
'КОРЧАГИНА' : ['КОРЧАГИНА', 'КОЧАГИНА', 'ПАВЛА', 'КАРЧАГИНА', 'П.КОРЧАГИНА'],
'ЛЕБЕДИНЫЙ' : ['ЛЕБЕДИН'],
'М-ГВАРДИИ' : ['ГВАРДИИ', 'ГВАРДИЯ'],
'НАБЕРЕЖНЫЙ' : ['НАБЕРЕЖН'],
'Р-Н АВТОВОКЗАЛА' : ['АВТОВОК'],
'РЕЧНОЙ' : ['РЕЧН'],
'СЕВЕРНЫЙ' : ['СЕВЕРН'],
'СОЛНЕЧНЫЙ' : ['СОЛНЕЧН'],
'УКРАИНСКИЙ' : ['УКРАИНСКИЙ'],
'ЧАЙКИНОЙ' : ['ЧАЙКИН'],
'ШАХТЕРСКИЙ' : ['ШАХТЕРСК'],
'ДРУЖБЫ' : ['ДРУЖБА'], 
'ЛАЗО' : ['СЕРГЕЯ ЛАЗО'],
'Д-БЕДНОГО' : ['ПЕРЕУЛОК Д.БЕДНОГО'],
'РЯБИНОВЫЙ': ['ПЕРЕУЛОК РЯБИНОВЫЙ']
}
i = 2
ik = 2
ig = 2


while (sheet.Cells(i,1).value != None):

	sheet.Cells(i,5).value = sheet.Cells(i,5).value.upper()
	sheet.Cells(i,6).value = sheet.Cells(i,6).value.upper()
	sheet.Cells(i,5).value = (sheet.Cells(i,5).value.translate({ord(x): y for (x, y) in zip(frm, to)}))
	sheet.Cells(i,6).value = (sheet.Cells(i,6).value.translate({ord(x): y for (x, y) in zip(frm, to)}))
	#print(str(sheet.Cells(i,11).value))
	
	#("{0:.0f}".format(round(df.loc[index, 'Показание'],0)))
	#("{0:.0f}".format(round(int(sheet.Cells(i,11).value,0)))
	#print (str("{0:.0f}".format(round(int(sheet.Cells(i,11).value,0)))))
	if str(sheet.Cells(i,11).value) in podraz.keys():
		sheet.Cells(i,12).value = podraz.get(str(sheet.Cells(i,11).value))

	ulica = sheet.Cells(i,2).value
	
	
	
	if ulica in kachar:
		while (sheetItog.Cells(ig,1).value != None):
			if float(sheet.Cells(i,11).value) == float(sheetItog.Cells(ig,1).value):
				sheetItog.Cells(ig,4).value += 1
				sheetItog.Cells(ig,8).value += 1
				sheetItog.Cells(ig,7).value += sheet.Cells(i,7).value
				break
			ig += 1	
		l = [r[0].value for r in sheet.Range("A"+str(i)+":L"+str(i))]
		j = 1
		while j < 13:
			sheetKach.Cells(ik,j).value = l[j-1]
			j+=1
		ik += 1
		sheet.Rows(i).EntireRow.Delete()
		ig = 2
	else:
		while (sheetItog.Cells(ig,1).value != None):
			if float(sheet.Cells(i,11).value) == float(sheetItog.Cells(ig,1).value):
				sheetItog.Cells(ig,4).value += 1
				sheetItog.Cells(ig,6).value += 1
				sheetItog.Cells(ig,5).value += sheet.Cells(i,7).value
				break
			ig += 1	
		
		for key in slovul.keys():
			for ull in slovul[key]:
				if ull in ulica:
					(sheet.Cells(i,2).value) = key
					break

		if sheet.Cells(i,2).value in resultsUl:
			ul = str(resultsUl[sheet.Cells(i,2).value]).zfill(4)

			if isinstance((sheet.Cells(i,3).value),float):
				dom = str(int(sheet.Cells(i,3).value)).zfill(6)
			else:
				sheet.Cells(i,3).value = sheet.Cells(i,3).value.upper()
				sheet.Cells(i,3).value = sheet.Cells(i,3).value.replace('A','А')
				dom = str(sheet.Cells(i,3).value).zfill(6)
			
			
			if sheet.Cells(i,4).value == None or sheet.Cells(i,4).value == 'NULL':
				sheet.Cells(i,4).value = 0
				kv = '0000'
			else:
				if isinstance((sheet.Cells(i,4).value),float):
					kv = str(int(sheet.Cells(i,4).value)).zfill(4)
				else:
					sheet.Cells(i,4).value = sheet.Cells(i,4).value.upper()
					sheet.Cells(i,4).value = sheet.Cells(i,4).value.replace('A','А')
					kv = str(sheet.Cells(i,4).value).zfill(4)
			
			sheet.Cells(i,13).value = str(ul + dom + kv)
		
		cursor1 = connection.cursor()
		mySQLQuery1 = ("""
					select ДоговорИд
					from таб_Договора 
					where АбонентИд =  '"""+ (sheet.Cells(i,13).value) + """' and Действующий = 1
					""")
		cursor1.execute(mySQLQuery1)

		resultsDog = cursor1.fetchall()
		try:
			(sheet.Cells(i,14).value) =  resultsDog[0][0]
		except IndexError:
			(sheet.Cells(i,14).value) = 'Нет адреса'	
		ig = 2
		i += 1


connection.close()

print("--- %s seconds ---" % (time.time() - start_time))

wb.Save()

#закрываем ее
wb.Close()

#закрываем COM объект
Excel.Quit()

print("--- %s seconds ---" % (time.time() - start_time))