import os 

import datetime
from datetime import date
from datetime import timedelta
from dateutil.relativedelta import relativedelta
import pandas as pd 
import win32com.client
Excel = win32com.client.Dispatch("Excel.Application")

df = []

#directoryF = '//Master/job/Касса/2018/Декабрь/11/'
#directoryU = '//Master/job/Банк/2018/декабрь/rudvodokanal_individual_11.12.2018.xls' 

dir = '//Master/job'
kassa = '/Касса'
bank = '/Банк'

months = {"01":"январь", "02":"февраль", "03":"март", "04":"апрель", "05":"май", "06":"июнь", "07":"июль", "08":"август", "09":"сентябрь", "10":"октябрь", "11":"ноябрь", "12":"декабрь"}

dateStart = '22.02.2022'
#'23.02.2022'

d0 = datetime.datetime.strptime(dateStart, '%d.%m.%Y')
d1 = datetime.datetime.now()
delta = d1 - d0
end_date = d0 + timedelta(days=1)

i = 1

while i <= int(delta.days):
	directory = dir + kassa + '/' + str(d0.strftime("%Y")) + '/' + months[str(d0.strftime("%m"))] + '/' + str(int(d0.strftime("%d"))) + '/'
	
	if os.path.isdir(directory):
		files = os.listdir(directory)
		excel = filter(lambda x: x.endswith('.xls'), files)
		excelFiz = filter(lambda x: x.find("Ю", 0,len(x)) == -1, excel)
		#excelFiz = filter(lambda x: x.find("$", 0,len(x)) == -1, excel)

		for f in excelFiz: 
			data = pd.read_excel(directory + f, 'Лист1')
			df.append(data)
	
	j = -1
	while j <= 1:
		dateprov = d0 + relativedelta(months=+j)
		directory = dir + bank + '/' + str(dateprov.strftime("%Y")) + '/' + months[str(dateprov.strftime("%m"))] + '/rudvodokanal_individual_' + str(d0.strftime("%d.%m.%Y")) + '_offline.xls'
		if os.path.isfile(directory):
			data = pd.read_excel(directory, 'Worksheet 1') 
			df.append(data)
			j = 1
		j = j + 1
	d0 = d0 + timedelta(days=1)
	i = i + 1

df = pd.concat(df) 
df = df.reset_index(drop=True)
#df = df.sort_values(['СчетчикИд', 'Показание'], axis=0, ascending=True)
df = df.sort_values(['СчетчикИд', 'Показание'], ascending=[True, False])
#df = df.sort_values(['СчетчикИд', 'Показание'])
df = df[pd.notnull(df['СчетчикИд'])]

#print(df['Показание']%1)

#print(df)

df = df.drop_duplicates(['СчетчикИд'], keep='last')
df = df.reset_index(drop=True)
df.columns = ['Улица', 'Дом', 'Кв', 'ДоговорИд', 'СчетчикИд', 'Показание', 'Оператор']

#print(df)

for index, row in df.iterrows():
	#print(row['Показание'])
	#print(row['Оператор'])
	#print(row['СчетчикИд'])
	if row['Показание']%1 > 0:
		df.loc[index, 'Показание'] = ("{0:.3f}".format(round(df.loc[index, 'Показание'],3))).zfill(9)
	else:
		df.loc[index, 'Показание'] = ("{0:.0f}".format(round(df.loc[index, 'Показание'],0))).zfill(5)

print(df)

df.to_excel('//laptem/Trans/Показания_Кассы.xls', 'Лист3')

wb = Excel.Workbooks.Open('//laptem/Trans/Показания_Кассы.xls')
sheet = wb.ActiveSheet
sheet.Cells(1,1).value = 'Номер'

wb.Save()

#закрываем ее
wb.Close()

#закрываем COM объект
Excel.Quit()


'''
i = 1
while i <= int(delta.days):
	j = -1
	while j <= 1:
		dateprov = d0 + relativedelta(months=+j)
		#print(dateprov )
		directory = dir + bank + '/' + str(dateprov.strftime("%Y")) + '/' + months[str(dateprov.strftime("%m"))] + '/rudvodokanal_individual_' + str(d0.strftime("%d.%m.%Y")) + '_offline'+ '.xls'
		print (directory)
		#print (str(dateprov.strftime("%m")))
		j = j + 1
	#print (directory)
	i = i + 1
'''
